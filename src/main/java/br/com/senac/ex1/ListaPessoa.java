package br.com.senac.ex1;

import br.com.senac.ex1.modelo.Pessoa;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListaPessoa {

    public static void main(String[] args) {

        List<Pessoa> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        Pessoa p = new Pessoa();
        System.out.println("Nome:");
        p.setNome(scanner.nextLine());
        System.out.println("Idade:");
        p.setIdade(scanner.nextInt());

        Pessoa p1 = new Pessoa();
        System.out.println("Nome:");
        p1.setNome(scanner.nextLine());
        System.out.println("Idade:");
        p1.setIdade(scanner.nextInt());

        Pessoa p2 = new Pessoa();
        System.out.println("Nome:");
        p2.setNome(scanner.nextLine());
        System.out.println("Idade:");
        p2.setIdade(scanner.nextInt());

        lista.add(p);
        lista.add(p1);
        lista.add(p2);

        Pessoa pVelha = getPessoaMaisVelha(lista);
        Pessoa pNova = getPessoaMaisNova(lista);
        System.out.println("Pessoa Mais Velha:" + pVelha.getNome());
        System.out.println("Pessoa Mais Nova:" + pNova.getNome());

    }

    public static Pessoa getPessoaMaisNova(List<Pessoa> lista) {
        Pessoa pessoaMaisNova = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() < pessoaMaisNova.getIdade()) {
                pessoaMaisNova = p;
            }
        }

        return pessoaMaisNova;
    }

    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista) {

        Pessoa pessoaMaisVelha = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() > pessoaMaisVelha.getIdade()) {
                pessoaMaisVelha = p;
            }
        }
        return pessoaMaisVelha;
    }

}
